from django import forms
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = {
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",

        }


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = {
            "name",
        }


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = {
            "number",
            "name",
        }