from django.shortcuts import render, redirect
from receipts.form import AccountForm, ExpenseForm, ReceiptForm
from receipts.models import Account, ExpenseCategory, Receipt
from django.views.generic import View
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser = request.user) 
    context = {
        "receipt": receipt
    }
    return render(request, "receipts/receipts_list.html", context)

@login_required 
def receipt_create(request):
    context = {}
    form = ReceiptForm(request.POST or None)
    if form.is_valid(): 
        receipts = form.save(commit=False)
        receipts.purchaser = request.user
        receipts.save()
        return redirect("home")
    context['form'] = form
    return render(request, "receipts/receipts_create.html", context)

@login_required
def catagory_list(request):
    expense = ExpenseCategory.objects.filter(owner = request.user) 
    context = {
        "expense": expense
    }
    return render(request, "receipts/expense_list.html", context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner = request.user) 
    context = {
        "account": account
    }
    return render(request, "receipts/account_list.html", context)


@login_required 
def expense_create(request):
    context = {}
    form = ExpenseForm(request.POST or None)
    if form.is_valid(): 
        expense = form.save(commit=False)
        expense.owner = request.user
        expense.save()
        return redirect("catagory_list")
    context['form'] = form
    return render(request, "receipts/receipts_create.html", context)

@login_required 
def account_create(request):
    context = {}
    form = AccountForm(request.POST or None)
    if form.is_valid(): 
        expense = form.save(commit=False)
        expense.owner = request.user
        expense.save()
        return redirect("account_list")
    context['form'] = form
    return render(request, "receipts/accounts_create.html", context)

    




