from django.contrib import admin
from django.urls import path, include
from receipts.views import (
    account_create,
    account_list,
    catagory_list,
    receipt_list,
    receipt_create,
    expense_create,
    account_create
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("categories/", catagory_list, name="catagory_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", expense_create, name="expense_create"),
    path("accounts/create/", account_create, name="create_account")
]
